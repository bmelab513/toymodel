import tensorflow as tf 
from keras.backend import set_session
import os
import numpy as np 
import argparse
import logging
import time
import model
from DataGenerator import DataGenerator
from util import set_logger,step_decay,convert_label_to_dict
from util import Params,Load_data
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import CSVLogger,ModelCheckpoint,LearningRateScheduler
from keras.utils import to_categorical
from keras import models
from keras import layers
from keras import optimizers
from sklearn.utils.class_weight import compute_class_weight

os.environ["CUDA_VISIBLE_DEVICES"]="0"
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.9
sess = tf.Session(config=config)
set_session(sess)

set_logger('train.log')
parser = argparse.ArgumentParser()
parser.add_argument("--params_dir", help="Directory to the params file",default='params.json')


# Retrieve parameters
args = parser.parse_args()
params = Params(args.params_dir)
data_dir = params.train_dir
eval_size = params.eval_size
label_dir = params.train_label_dir
val_dir = params.val_dir
val_label_dir = params.val_label_dir
model_name = params.model_name
num_epoch = params.num_epoch
image_dim = (params.image_dim,params.image_dim,3)
batch_size = params.batch_size
num_outputs = params.num_outputs
lr = params.lr
save_weight_at = params.save_weight_at
save_log_at = params.save_log_at

# Set params for the generator

generator_params = {'dim':image_dim,
          'batch_size':batch_size,
          'n_classes':num_outputs,
          'shuffle':True,
         'brightness_range':(-0.3,0.3),
         'contrast_range':(-0.2,0.2)}

# Get the files
train_loader = Load_data(data_dir,label_dir,eval_size)
train_files = train_loader.get_image_files()
train_names = train_loader.get_names()
train_labels = train_loader.get_labels()

# split into train and test partion if eval_size is specified, else load data from val_dir
data = {}
if eval_size:
    X_train,X_val,y_train,y_val = train_loader.train_test_split()
    data['train'] = X_train
    data['val'] = X_val
    train_labels = y_train
    val_labels = y_val
    train_label_dict = convert_label_to_dict(label_dir) #convert the label to dictionary
    val_label_dict = convert_label_to_dict(label_dir)
    train_datagen = DataGenerator(data['train'],train_label_dict,data_dir=data_dir,**generator_params)
    val_datagen = DataGenerator(data['val'],val_label_dict,test=True,data_dir=data_dir,**generator_params)
else:
    data['train'] = train_names
    val_loader = Load_data(val_dir,val_label_dir,eval_size=None)
    val_names = val_loader.get_names()
    val_labels = val_loader.get_labels()
    data['val'] = val_names
    train_label_dict = convert_label_to_dict(label_dir) #convert the label to dictionary
    val_label_dict = convert_label_to_dict(val_label_dir)
    train_datagen = DataGenerator(data['train'],train_label_dict,data_dir=data_dir,**generator_params)
    val_datagen = DataGenerator(data['val'],val_label_dict,test=True,data_dir=val_dir,**generator_params)


# Compute the class weight
class_weights = compute_class_weight(class_weight='balanced',classes=np.unique(train_labels),y=train_labels)
class_weights_dict = dict(zip(np.unique(train_labels),class_weights))
print("Class weights: ",class_weights_dict)

# Load model
if model_name == 'inceptionv3':
    model = model.inceptionv3(num_outputs,image_dim)
if model_name == 'inception_resnet_v2':
    model = model.incepption_resnet_v2(num_outputs,image_dim)

print(model.summary())

# Callbacks
saved_weight_name = os.path.join(save_weight_at,'weights_{}_epoch_{}_batchsize_{}_lr_{}_epochdrop_5.h5'.format(model_name,num_epoch,batch_size,lr))
model_checkpoint = ModelCheckpoint(filepath=saved_weight_name,verbose=1,save_best_only=True)
csv_logger = CSVLogger(os.path.join(save_log_at,'log_{}_epoch_{}_batchsize_{}_lr_{}_epochdrop_5.csv'.format(model_name,num_epoch,batch_size,lr)))
lr_scheduler = LearningRateScheduler(step_decay)
callbacks = [lr_scheduler]

# Compile and train 
model.compile(loss='binary_crossentropy',optimizer=optimizers.Adam(lr=lr),metrics=['accuracy'])
logging.info("Start fitting")
start = time.time()
history = model.fit_generator(generator=train_datagen,
							  epochs=num_epoch,steps_per_epoch=100,
							  validation_data=val_datagen,
                              validation_steps=50,
                              callbacks=callbacks,
                              use_multiprocessing=True,
                              workers=6,
                              class_weight=class_weights_dict)
end = time.time()
logging.info("Time elapsed for fitting: " + str(end-start) + " seconds")
