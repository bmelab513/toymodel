from __future__ import division, print_function
import os
import logging
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import skimage.io as io
import skimage.transform as transform

from skimage.transform._warps_cy import _warp_fast
from sklearn.utils import shuffle
from sklearn import model_selection
from glob import glob
from PIL import Image

class Params():
    """Class that loads hyperparameters from a json file.
    Example:
    ```
    params = Params(json_path)
    print(params.learning_rate)
    params.learning_rate = 0.5  # change the value of learning_rate in params
    ```
    """

    def __init__(self, json_path):
        self.update(json_path)

    def save(self, json_path):
        """Saves parameters to json file"""
        with open(json_path, 'w') as f:
            json.dump(self.__dict__, f, indent=4)

    def update(self, json_path):
        """Loads parameters from json file"""
        with open(json_path) as f:
            params = json.load(f)
            self.__dict__.update(params)

    @property
    def dict(self):
        """Gives dict-like access to Params instance by `params.dict['learning_rate']`"""
        return self.__dict__

class Load_data():
    
    def __init__(self,data_dir,label_file,eval_size):
        
        self.data_dir = data_dir
        self.label_file = label_file
        self.eval_size = eval_size
        
    def get_labels(self):
        """
        The function to return labels of the image files given the csv label file
        Args:
            names: (list) image names (eg.['left_10','right_10'..]) returned from fuction get_names
            label_file: (string) path to the label file
        Returns:
            labels: the labels in shape of (num_labels,)
        """
        labels = pd.read_csv(self.label_file,index_col=0).loc[self.get_names()].values.flatten()
        return labels

    def get_image_files(self):
        """
        Return the an array of all images files with full path within a folder

        Args:
            datadir: string, path to where the dataset locates
            left_only

        Returns:
            fs: an array of image files

        """
        fs = glob('{}/*'.format(self.data_dir))

        return np.array(sorted(fs))


    def get_names(self):
        """
        Return the names of images within a set
        Args:
            files: array of image files with full path returned from function get_image_files
        Returns:
            a list of image names (E.g. ['10_left','10_right'...])
        """
        files = self.get_image_files()

        return [os.path.basename(x).split('.')[0] for x in files]

    def load_image(self):
        """
        Load image into an array
        Args:
            files: array of paths to the images returned from function get_files
        Returns:
            an array of load images in shape of (num_images,H,W,C)

        """
        fname = self.get_image_files()
        X = np.empty((len(fname),512,512,3))
        for i in range(len(fname)):
            img = io.imread(fname[i])
            X[i,] = crop_and_resize(img,(512,512,3))
        
        return X.astype(np.float16)

    def split_indices(self,random_state=21):
        """
        Split the train and testset using Stratified Shuffle Split from sklearn
        Args:
            files: names of images returned from function get_names
            labels: labels of the images returned from function get_labels
            label_dir: path to the label file (E.g.:./trainLabels.csv)
            test_size: percentage of trainset to be split into validation set
            random_state: number for randomness
        Returns:
            tr, te: indices for training and validation set
        """
        names = self.get_names()
        labels = self.get_labels()
        spl = model_selection.StratifiedShuffleSplit(n_splits=1, 
                                test_size=self.eval_size, 
                                random_state=random_state)
        tr,te = next(iter(spl.split(names,labels)))
        #tr = np.hstack([tr * 2, tr * 2 + 1])
        #te = np.hstack([te * 2, te * 2 + 1])
        return tr, te

    def split(self,files,labels):
        """
        Split into train and testset
        Args:
            files: names of the images returned from the function get_names
            labels: labels of the images returned from function get_labels
            label_file: path to the label file (E.g. ./trainLabels.csv)
            test_size: percentage of trainset to be split into validation set
            random_state: number for randomness
        Returns:
            list of image paths and labels for train and validation set
        """
        train, test = self.split_indices()
        files = np.array(files)
        labels = np.array(labels)
        return files[train], files[test], labels[train], labels[test]

    def train_test_split(self):
        """ 
        This function calls the 'split' function for spliting the train and test set
        Args:
            X: name of the images returned from the function get_names
            y: labels of the images returned from the function get_labels
            eval_size: percentage of trainset to be split into validation set
        Returns:
            list of image paths and labels for train and validation set
        """
        names = self.get_names()
        labels = self.get_labels()
        if self.eval_size:
            X_train, X_valid, y_train, y_valid = self.split(names,labels)
        else:
            X_train, y_train = names, labels
            X_valid, y_valid = names[len(names):], labels[len(labels):]

        return X_train, X_valid, y_train, y_valid

def set_logger(log_path):
    """Sets the logger to log info in terminal and file `log_path`.
    In general, it is useful to have a logger so that every output to the terminal is saved
    in a permanent file. Here we save it to `model_dir/train.log`.
    Example:
    ```
    logging.info("Starting training...")
    ```
    Args:
        log_path: (string) where to log
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    if not logger.handlers:
        # Logging to a file
        file_handler = logging.FileHandler(log_path)
        file_handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s: %(message)s'))
        logger.addHandler(file_handler)

        # Logging to console
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(logging.Formatter('%(message)s'))
        logger.addHandler(stream_handler)

def plot_result(csv_file):

    record = pd.read_csv(csv_file)
    acc = record['acc']
    val_acc = record['val_acc']
    loss = record['loss']
    val_loss = record['val_loss']

    epochs = range(len(acc))

    plt.plot(epochs, acc, 'r', label='Training acc')
    plt.plot(epochs, val_acc, 'b', label='Validation acc')
    plt.title('Training and validation accuracy')
    plt.legend()
    plt.savefig('accuracy.png')

    plt.figure()

    plt.plot(epochs, loss, 'r', label='Training loss')
    plt.plot(epochs, val_loss, 'b', label='Validation loss')
    plt.title('Training and validation loss')
    plt.legend()

    plt.savefig('loss.png')

def step_decay(epoch):
    """ Functions to decay the learning rate after certain epochs by lr = lr_0*drop**(floor(total_epoch/epoch))
    
    Args:
        epoch: the current epoch fed from the keras LearningRateScheduler
    Returns:
        lrate: the new decayed learning rate
    
    """
    initial_lr = 5e-5
    drop = 0.8
    epochs_drop = 5
    lrate = initial_lr * math.pow(drop,  
           math.floor((1+epoch)/epochs_drop))
    return lrate

def convert_label_to_dict(label_file):
    """
    Convert the labels into dictionary for input to the generator
    For example: label_dict = {'10_left':0,'10_right':1...}

    Args:
        label_dir: (string) path to the label file (E.g. ./trainLabels.csv)
    Returns:
        label_dict: a dictionary mapping each image to the label
    """
    labels = pd.read_csv(label_file)
    images = labels['image']
    levels = labels['level']
    label_dict = dict(zip(images,levels))

    return label_dict

def crop_and_resize(img,crop_size):
    h = img.shape[0]
    w = img.shape[1]
    if w/float(h) >= 1.3:
        cols_thres = np.where(np.max(np.max(img,axis=2),axis=0)>35)[0]
        if len(cols_thres) > crop_size[0]//2:
            min_x,max_x = cols_thres[0],cols_thres[-1]
        else: 
            min_x, max_x = 0,-1
        converted = img[0:h,min_x:max_x]

    else:
        converted = img

    trans = transform.resize(converted,crop_size,mode='constant')

    return trans