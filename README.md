# Toy Model for Eye Screening System

### Installation

Extract train/test images to `data/train` and `data/test` respectively and
put the `trainLabels.csv` file into the `data` directory as well.

If you'd like to run a deterministic variant, install python3 dependencies via,
```
conda install --yes --file requirements.txt
```

For GPU, 

```
conda install --yes --file requirements_gpu.txt
```

### Usage
#### Training a toy model

#### Scripts
All these python scripts can be invoked with `--help` to display a brief help
message. 

- `model.py` store used models
- `train.py` trains convolutional networks

##### train.py
Example usage:
```
python train.py --data_dir data/train --label_dir data/trainLabels.csv --eval_size 0.1
```
Usage: train.py [OPTIONS]

Options:
  --data_dir TEXT       Path or name of configuration module.
  --label_dir TEXT      Path to initial weights file.
  --eval_size FLOAT     portion of validation data
  --help                Show this message and exit.