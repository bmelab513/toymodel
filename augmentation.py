import numpy as np 
import skimage.io as io
import matplotlib.pyplot as plt
import math
import cv2
from skimage import transform
from sklearn.preprocessing import normalize


def random_rotation(img):
    # pick a random degree of rotation between 25% on the left and 25% on the right
    random_degree = random.uniform(-25, 25)
    
    return transform.rotate(img, random_degree)


def horizontal_flip(img):
    # horizontal flip doesn't need skimage, it's easy as flipping the image array of pixels !
	return image_array[:, ::-1]



def contrast_enhance(im, radius):
    """Subtract local average color and map local average to 50% gray
    Parameters
    ==========
    im: array of shape (height, width, 3)
    radius: int
        for square images a good choice is size/2
    Returns
    =======
    im_ce: contrast enhanced image as array of shape (height, width, 3)
    Reference
    =========
    B. Graham, "Kaggle diabetic retinopathy detection competition report",
        University of Warwick, Tech. Rep., 2015
    https://github.com/btgraham/SparseConvNet/blob/kaggle_Diabetic_Retinopathy_competition/Data/kaggleDiabeticRetinopathy/preprocessImages.py
    """

    radius = int(radius)
    b = np.zeros(im.shape)
    cv2.circle(b, (radius, radius), int(radius * 0.9),
               (1, 1, 1), -1, 8, 0)
    im_ce = cv2.addWeighted(im, 4,
                            cv2.GaussianBlur(im, (0, 0), radius / 30),
                            -4, 128) * b + 128 * (1 - b)
    return im_ce


def transformation(img,radius=512//2):
	img = contrast_enhance(img,radius)
	# rescale into [0,1]
	# img = cv2.normalize(img, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

	return img