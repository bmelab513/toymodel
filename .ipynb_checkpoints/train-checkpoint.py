import os
import numpy as np 
import argparse
import logging
import time
import model
from DataGenerator import DataGenerator
from util import get_labels, get_image_files, get_names, load_image, train_test_split,set_logger, plot_result, convert_label_to_dict
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import CSVLogger,ModelCheckpoint
from keras.utils import to_categorical
from keras import models
from keras import layers
from keras import optimizers
from sklearn.utils.class_weight import compute_class_weight

set_logger('train.log')
parser = argparse.ArgumentParser()
parser.add_argument("--data_dir", help="Directory to the training data folder",default='data/train')
parser.add_argument("--eval_size",help="Size of validation set",default=0.1)
parser.add_argument("--label_dir",help="Directory to the label file",default='data/trainLabels.csv')
parser.add_argument("--saved_model",help="Name of the model to be saved",default='model.hdf5')

args = parser.parse_args()
data_dir = args.data_dir
eval_size = args.eval_size
label_dir = args.label_dir
saved_model = args.saved_model
files = get_image_files(data_dir)
names = get_names(files)
labels = get_labels(names=names,label_file=label_dir)

# split into train and test partion
data = {}
X_train,X_val,y_train,y_val = train_test_split(names,labels,eval_size)
data['train'] = X_train
data['val'] = X_val

# Set params for the generator

params = {'dim':(512,512,3),
          'batch_size':16,
          'n_classes':5,
          'shuffle':True,
          'data_dir':data_dir}

# Compute the class weight
class_weights = compute_class_weight(class_weight='balanced',classes=np.unique(labels),y=labels)
class_weights_dict = dict(zip(np.unique(labels),class_weights))
print("Class weights: ",class_weights_dict)

# Load model
model = model.inceptionv3(params['n_classes'],params['dim'])
print(model.summary())
label_dict = convert_label_to_dict(label_dir) #convert the label to dictionary
train_datagen = DataGenerator(data['train'],label_dict,**params)
val_datagen = DataGenerator(data['val'],label_dict,**params)

# Callbacks
model_checkpoint = ModelCheckpoint(filepath=saved_model)
csv_logger = CSVLogger('training.csv')
callbacks = [model_checkpoint,csv_logger]

# Compile and train 
model.compile(loss='categorical_crossentropy',optimizer=optimizers.Adam(lr=1e-5),metrics=['accuracy'])
logging.info("Start fitting")
start = time.time()
history = model.fit_generator(generator=train_datagen,
							  epochs=20,
							  validation_data=val_datagen,
                              callbacks=callbacks,
                              use_multiprocessing=True,
                              workers=6)
end = time.time()
logging.info("Time elapsed for fitting: " + str(end-start) + " seconds")
