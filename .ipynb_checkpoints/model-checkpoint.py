from keras import models
from keras import layers
from keras.applications.resnet50 import ResNet50
from keras.applications.inception_v3 import InceptionV3

def simple_model(input_shape):
    X_input = models.Input(shape=input_shape)
    X = layers.Conv2D(filters=96,kernel_size=11,strides=4,activation='relu')(X_input)
    X = layers.BatchNormalization()(X)
    X = layers.MaxPooling2D(pool_size=3,strides=2)(X)
    
    X = layers.Conv2D(filters=256,kernel_size=5,strides=1,padding='same',activation='relu')(X)
    X = layers.BatchNormalization()(X)
    X = layers.MaxPooling2D(pool_size=3,strides=2)(X)
    
    X = layers.Conv2D(filters=384,kernel_size=3,strides=1,padding='same',activation='relu')(X)
    X = layers.Conv2D(filters=384,kernel_size=3,strides=1,padding='same',activation='relu')(X)
    X = layers.Conv2D(filters=256,kernel_size=3,strides=1,padding='same',activation='relu')(X)
    X = layers.BatchNormalization()(X)
    X = layers.MaxPooling2D(pool_size=3,strides=2)(X)
    
    X = layers.GlobalMaxPooling2D()(X)
    X = layers.Dense(5,activation='softmax')(X)
    
    model = models.Model(inputs=X_input, outputs=X)
    
    return model

def resnet50(num_outputs,input_shape):
    base_model = ResNet50(include_top=False,weights=None,input_shape=input_shape)
    model = models.Sequential()
    model.add(base_model)
    model.add(layers.GlobalAveragePooling2D())
    model.add(layers.Dense(1024,activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(1024,activation='relu'))
    model.add(layers.Dense(num_outputs,activation='softmax'))

    return model
 
def inceptionv3(num_outputs,input_shape):
    base_model = InceptionV3(include_top=False,weights=None,input_shape=input_shape)
    model = models.Sequential()
    model.add(base_model)
    model.add(layers.GlobalAveragePooling2D())
    model.add(layers.Dense(1024,activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(1024,activation='relu'))
    model.add(layers.Dense(num_outputs,activation='softmax'))

    return model