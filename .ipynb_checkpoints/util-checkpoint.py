from __future__ import division, print_function
import os
import logging
from glob import glob
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from PIL import Image
import skimage
import skimage.transform
from skimage.transform._warps_cy import _warp_fast
from sklearn.utils import shuffle
from sklearn import model_selection


RANDOM_STATE = 21
def get_labels(names, labels=None, label_file='/home/lab513/teamOo/kaggle_diabetic/data/trainLabels.csv', 
               per_patient=False):
    
    if labels is None:
        labels = pd.read_csv(label_file, 
                             index_col=0).loc[names].values.flatten()

    if per_patient:
        left = np.array(['left' in n for n in names])
        return np.vstack([labels[left], labels[~left]]).T
    else:
        return labels
    
def convert_label_to_dict(label_dir):
    labels = pd.read_csv(label_dir)
    images = labels['image']
    levels = labels['level']
    label_dict = dict(zip(images,levels))
    return label_dict
        
    return label_dict
def get_image_files(datadir, left_only=False):
    fs = glob('{}/*'.format(datadir))
    if left_only:
        fs = [f for f in fs if 'left' in f]
    return np.array(sorted(fs))


def get_names(files):
    return [os.path.basename(x).split('.')[0] for x in files]


def load_image(fname):
    if isinstance(fname, str):
        return np.array(Image.open(fname), dtype=np.float32)
    else:
        return np.array([load_image(f) for f in fname])

def split_indices(files, labels, test_size=0.1, random_state=RANDOM_STATE):
    names = get_names(files)
    labels = get_labels(names, per_patient=False)
    print(labels.shape)
    spl = model_selection.StratifiedShuffleSplit(n_splits=1, 
                            test_size=test_size, 
                            random_state=random_state)
    tr,te = next(iter(spl.split(files,labels)))
    #tr = np.hstack([tr * 2, tr * 2 + 1])
    #te = np.hstack([te * 2, te * 2 + 1])
    return tr, te

def split(files, labels, test_size=0.1, random_state=RANDOM_STATE):
    train, test = split_indices(files, labels, test_size, random_state)
    files = np.array(files)
    labels = np.array(labels)
    return files[train], files[test], labels[train], labels[test]

def train_test_split(X, y, eval_size):
    if eval_size:
        X_train, X_valid, y_train, y_valid = split(
                X, y, test_size=eval_size)
    else:
        X_train, y_train = X, y
        X_valid, y_valid = X[len(X):], y[len(y):]

    return X_train, X_valid, y_train, y_valid

def set_logger(log_path):
    """Sets the logger to log info in terminal and file `log_path`.
    In general, it is useful to have a logger so that every output to the terminal is saved
    in a permanent file. Here we save it to `model_dir/train.log`.
    Example:
    ```
    logging.info("Starting training...")
    ```
    Args:
        log_path: (string) where to log
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    if not logger.handlers:
        # Logging to a file
        file_handler = logging.FileHandler(log_path)
        file_handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s: %(message)s'))
        logger.addHandler(file_handler)

        # Logging to console
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(logging.Formatter('%(message)s'))
        logger.addHandler(stream_handler)

def plot_result(history):


	acc = history.history['acc']
	val_acc = history.history['val_acc']
	loss = history.history['loss']
	val_loss = history.history['val_loss']

	epochs = range(len(acc))

	plt.plot(epochs, acc, 'r', label='Training acc')
	plt.plot(epochs, val_acc, 'b', label='Validation acc')
	plt.title('Training and validation accuracy')
	plt.legend()
	plt.savefig(accuracy.png)

	plt.figure()

	plt.plot(epochs, loss, 'r', label='Training loss')
	plt.plot(epochs, val_loss, 'b', label='Validation loss')
	plt.title('Training and validation loss')
	plt.legend()

	plt.savefig('loss.png')

