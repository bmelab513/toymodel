from __future__ import division
from sklearn.metrics import confusion_matrix

import numpy as np 


def true_positives(conf_matrix):
	"""Find the true positives from the confusion matrix
	Args:
		conf_matrix: confusion matrix in size of nxn
	Outputs:
		tp: a list of true positives for each class in a confusion matrix
	"""
	classes = len(conf_matrix)
	tp = []
	for i in range(classes):
		tp.append(conf_matrix[i,i])

	return tp

def false_postives(conf_matrix):
	"""Find the false positives from the confusion matrix
	Args:
		conf_matrix: confusion matrix in size of nxn
	Outputs:
		fp: a list of false positives for each class in a confusion matrix
	"""
	classes = len(conf_matrix)
	fp = []
	for i in range(classes):
		fp.append(np.sum(conf_matrix[:,i]) - conf_matrix[i,i])

	return fp

def false_negatives(conf_matrix):
	"""Find the false negatives from the confusion matrix
	Args:
		conf_matrix: confusion matrix in size of nxn
	Outputs:
		fn: a list of false negatives for each class in a confusion matrix
	"""
	classes = len(conf_matrix)
	fn = []
	for i in range(classes):
		fn.append(np.sum(conf_matrix[i,:]) - conf_matrix[i,i])

	return fn

def true_negatives(conf_matrix):
	"""Find the true negatives from the confusion matrix
	Args:
		conf_matrix: confusion matrix in size of nxn
	Outputs:
		tn: a list of true positives for each class in a confusion matrix
	"""
	classes = len(conf_matrix)
	tn = []
	for i in range(classes):
		tn.append(np.sum(conf_matrix) - np.sum(conf_matrix[i,:]) - np.sum(conf_matrix[:,i]) + conf_matrix[i,i])

	return tn

def find_elements(conf_matrix):
	""" Find TP, FP, FN and TN from the confusion matrix
	Args:
		conf_matrix: confusion matrix in size of nxn
	Outputs:
		tp: list of true positives
		fp: list of false postives
		fn: list of false negatives
		tn: list of true negatives
	"""
	tp = true_positives(conf_matrix)
	fp = false_postives(conf_matrix)
	fn = false_negatives(conf_matrix)
	tn = true_negatives(conf_matrix)

	return tp,fp,fn,tn

def class_precision(conf_matrix):
	""" Find the precision for each class
	Args:
		conf_matrix: confusion matrix in size of nxn
	Outputs:
		precision: list of precision for each class
	"""
	tp = np.array(true_positives(conf_matrix))
	fp = np.array(false_postives(conf_matrix))
	precision = tp / (tp+fp)

	return precision

def class_recall(conf_matrix):
	""" Find the recall for each class
	Args:
		conf_matrix: confusion matrix in size of nxn
	Outputs:
		recall: list of recall for each class
	"""
	tp = np.array(true_positives(conf_matrix))
	fn = np.array(false_negatives(conf_matrix))

	recall = tp / (tp+fn)

	return recall

def class_specificty(conf_matrix):
	""" Find the specificty for each class
	Args:
		conf_matrix: confusion matrix in size of nxn
	Outputs:
		specificity: list of specificity for each class
	"""
	tn = np.array(true_negatives(conf_matrix))
	fp = np.array(false_postives(conf_matrix))

	specificity = tn / (tn+fp)

	return specificity	

def precision(conf_matrix,average='micro'):
	""" Find the precision for all class
	Args:
		conf_matrix: confusion matrix in size of nxn
		avarage (string): 'micro', 'macro' or 'binary'
	Outputs:
		precision: precision for all class
	"""
	if average == 'micro':		
		tp = np.array(true_positives(conf_matrix))
		fp = np.array(false_postives(conf_matrix))
		precision = np.sum(tp) / (np.sum(tp) + np.sum(fp))

	if average == 'macro':
		class_precs = class_precision(conf_matrix)
		precision = np.mean(class_precs)
	if average == 'binary':
		tp = conf_matrix[1,1]
		fp = conf_matrix[0,1]
		precision = tp/ (tp + fp)

	return precision

def recall(conf_matrix,average='micro'):
	""" Find the recall for all class
	Args:
		conf_matrix: confusion matrix in size of nxn
		average (str): 'micro','macro' or 'binary'
	Outputs:
		recall: recall for all class
	"""
	if average == 'micro':
		tp = np.array(true_positives(conf_matrix))
		fn = np.array(false_negatives(conf_matrix))
		recall = np.sum(tp) / (np.sum(tp) + np.sum(fn))
	if average == 'macro':
		class_rec = class_recall(conf_matrix)
		recall = np.mean(class_rec)
	if average == 'binary':
		tp = conf_matrix[1,1]
		fn = conf_matrix[1,0]
		recall = tp/(tp + fn)

	return recall

def specificity(conf_matrix,average='micro'):
	""" Find the specificity for all class
	Args:
		conf_matrix: confusion matrix in size of nxn
		average (str): 'micro','macro' or 'binary'
	Outputs:
		specificity: specificity for all class
	"""
	if average == 'micro':
		tn = np.array(true_negatives(conf_matrix))
		fp = np.array(false_postives(conf_matrix))
		specificity = np.sum(tn) / (np.sum(tn) + np.sum(fp))
	if average == 'macro':
		class_specs = class_specificty(conf_matrix)
		specificity = np.mean(class_specs)
	if average == 'binary':
		tn = conf_matrix[0,0]
		fp = conf_matrix[0,1]
		specificity = tn/ (tn + fp)

	return specificity

def accuracy(conf_matrix):
	""" Find the accuracy
	Args:
		conf_matrix: confusion matrix in size of nxn
	Outputs:
		accuracy: the accuracy
	"""
	tp = np.array(true_positives(conf_matrix))
	accuracy = np.sum(tp) / (np.sum(conf_matrix))

	return accuracy
