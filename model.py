from keras import models
from keras import layers
from keras.applications.resnet50 import ResNet50
from keras.applications.inception_v3 import InceptionV3
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.applications.densenet import DenseNet121,DenseNet169,DenseNet201

def simple_model(num_outputs,input_shape):
    X_input = models.Input(shape=input_shape)
    X = layers.Conv2D(filters=5,kernel_size=11,strides=4,activation='relu')(X_input)
    X = layers.BatchNormalization()(X)
    X = layers.MaxPooling2D(pool_size=3,strides=2)(X)
    
    X = layers.Conv2D(filters=10,kernel_size=5,strides=1,padding='same',activation='relu')(X)
    X = layers.BatchNormalization()(X)
    X = layers.MaxPooling2D(pool_size=3,strides=2)(X)
    
    X = layers.Conv2D(filters=20,kernel_size=3,strides=1,padding='same',activation='relu')(X)
    X = layers.Conv2D(filters=20,kernel_size=3,strides=1,padding='same',activation='relu')(X)
    X = layers.Conv2D(filters=20,kernel_size=3,strides=1,padding='same',activation='relu')(X)
    X = layers.BatchNormalization()(X)
    X = layers.MaxPooling2D(pool_size=3,strides=2)(X)
    
    X = layers.GlobalMaxPooling2D()(X)
    X = layers.Dense(num_outputs,activation='sigmoid')(X)
    
    model = models.Model(inputs=X_input, outputs=X)
    
    return model

def resnet50(num_outputs,input_shape):
    base_model = ResNet50(include_top=False,weights=None,input_shape=input_shape)
    model = models.Sequential()
    model.add(base_model)
    model.add(layers.GlobalAveragePooling2D())
    model.add(layers.Dense(512,activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(512,activation='relu'))
    model.add(layers.Dense(num_outputs,activation='sigmoid'))

    return model
 
def inceptionv3(num_outputs,input_shape):
    base_model = InceptionV3(include_top=False,weights='imagenet',input_shape=input_shape)
    model = models.Sequential()
    model.add(base_model)
    model.add(layers.GlobalAveragePooling2D())
    model.add(layers.Dense(512,activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(512,activation='relu'))
    model.add(layers.Dense(num_outputs,activation='sigmoid'))

    return model

def inception_resnet_v2(num_outputs,input_shape):
    base_model = InceptionResNetV2(include_top=False,weights='imagenet',input_shape=input_shape)
    model = models.Sequential()
    model.add(base_model)
    model.add(layers.GlobalAveragePooling2D())
    model.add(layers.Dense(512,activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(512,activation='relu'))
    model.add(layers.Dense(num_outputs,activation='sigmoid'))

    return model

def densenet121(num_outputs,input_shape):
    base_model = DenseNet121(include_top=False,weights='imagenet',input_shape=input_shape)
    model = models.Sequential()
    model.add(base_model)
    model.add(layers.GlobalAveragePooling2D())
    model.add(layers.Dense(512,activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(512,activation='relu'))
    model.add(layers.Dense(num_outputs,activation='sigmoid'))

    return model

def densenet169(num_outputs,input_shape):
    base_model = DenseNet169(include_top=False,weights='imagenet',input_shape=input_shape)
    model = models.Sequential()
    model.add(base_model)
    model.add(layers.GlobalAveragePooling2D())
    model.add(layers.Dense(512,activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(512,activation='relu'))
    model.add(layers.Dense(num_outputs,activation='sigmoid'))

    return model

def densenet201(num_outputs,input_shape):
    base_model = DenseNet201(include_top=False,weights='imagenet',input_shape=input_shape)
    model = models.Sequential()
    model.add(base_model)
    model.add(layers.GlobalAveragePooling2D())
    model.add(layers.Dense(512,activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(512,activation='relu'))
    model.add(layers.Dense(num_outputs,activation='sigmoid'))

    return model

