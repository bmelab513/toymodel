import os
import numpy as np 
import argparse
import model
from keras.models import load_model
from util import Params,Load_data
from evaluation_metrics import precision, recall, specificity, accuracy
from sklearn.metrics import confusion_matrix
from keras import optimizers

parser = argparse.ArgumentParser()
parser.add_argument("--weight_file",help="Path to the weight file",default=None)

# retrieve paramters
args = parser.parse_args()
weight_file = args.weight_file
params_file = './params.json'
params = Params(params_file)
test_dir = params.test_dir
test_label_dir = params.test_label_dir
model_name = params.model_name
lr = params.lr
image_dim = (params.image_dim,params.image_dim,3)
num_outputs = params.num_outputs

# load data
test_loader = Load_data(test_dir,test_label_dir,eval_size=None)
X_test = test_loader.load_image()
#X_test = (X_test-np.mean(X_test,axis=0))/(np.std(X_test,axis=0)+1e-7)
y_test = test_loader.get_labels()
print("Shape of X_test: " + str(X_test.shape))
print("Shape of y_test: " + str(y_test.shape))
print("Class distribution:", np.bincount(y_test))

# load model
if model_name == 'inceptionv3':
    model = model.inceptionv3(num_outputs,image_dim)
if model_name == 'inception_resnet_v2':
    model = model.inception_resnet_v2(num_outputs,image_dim)

model.compile(loss='binary_crossentropy',optimizer=optimizers.RMSprop(lr=1e-4),metrics=['accuracy'])
print("Loading weights ...")
model.load_weights(weight_file)

# # test the result
# print(model.evaluate(X_test,y_test))
predict = model.predict(X_test)
#print(predict)
y_pred = (predict>0.5)*np.ones(predict.shape)

print('Predict: '); print(y_pred.T)
print('Labels: '); print(y_test)

# # Confusion matrix
conf_matrix = confusion_matrix(y_test,y_pred)
print(conf_matrix)

# # Metrics
precision_score = precision(conf_matrix,average='binary')
recall_score = recall(conf_matrix,average='binary')
specificity_score = specificity(conf_matrix,average='binary')
acc = accuracy(conf_matrix)

# # print the result
print("Precision score: " + str(precision_score))
print("Recall score: " + str(recall_score))
print("specificity: " + str(specificity_score))
print("Accuracy: " + str(acc))
