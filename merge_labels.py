import os
import pandas as pd
from util import Params

def get_lst_images(file_path):
    """
    Reads in all files from file path into a list.

    INPUT
        file_path: specified file path containing the images.

    OUTPUT
        List of image strings
    """
    return [i for i in os.listdir(file_path) if i != '.DS_Store']


if __name__ == '__main__':

    params_dir = 'params.json'
    params = Params(params_dir)
    train_dir = params.train_dir
    train_label_dir = params.train_label_dir
    trainLabels = pd.read_csv(train_label_dir)

    lst_imgs = get_lst_images(train_dir)

    new_trainLabels = pd.DataFrame({'image': lst_imgs})
    new_trainLabels['image2'] = new_trainLabels.image

    # Remove the suffix from the image names.
    new_trainLabels['image2'] = new_trainLabels.loc[:, 'image2'].apply(lambda x: '_'.join(x.split('_')[0:2]))

    # Strip and add .jpeg back into file name
    new_trainLabels['image2'] = new_trainLabels.loc[:, 'image2'].apply(
        lambda x: '_'.join(x.split('_')[0:2]) + '.tiff')
    merge_labels = new_trainLabels['image2']
    new_trainLabels['image2'] = [i.split('.')[0] for i in merge_labels]
    # trainLabels = trainLabels[0:10]
    new_trainLabels.columns = ['train_image_name', 'image']
    trainLabels = pd.merge(trainLabels, new_trainLabels, how='outer', on='image')
    trainLabels = trainLabels.dropna()
    # move train_label_image to the first column and remove jpeg
    trainLabels['image'] = trainLabels['train_image_name']
    trainLabels['image'] = [i.split('.')[0] for i in trainLabels['image']]
    trainLabels.drop(columns=['train_image_name'], axis=1, inplace=True)
    print(trainLabels)
    trainLabels.to_csv('./trainLabels_v2.csv', index=False, header=True)
