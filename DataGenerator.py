import numpy as np
import keras
import os
import skimage.io as io
from util import crop_and_resize
from imgaug import augmenters as iaa

#from augmentation import transformation
class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self, list_IDs, labels,data_dir,contrast_range,brightness_range, batch_size=32, dim=(32,32,32), n_channels=1,
                 n_classes=10, shuffle=True,test=False):
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        self.labels = labels
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()
        self.data_dir = data_dir
        self.brightness_range = brightness_range
        self.contrast_range = contrast_range
        self.test = test

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, y = self.__data_generation(list_IDs_temp)

        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        X = np.empty((self.batch_size, *self.dim))
        y = np.empty((self.batch_size), dtype=int)
        seq = iaa.Sometimes(0.5,iaa.Fliplr(0.5),
                            iaa.Flipud(0.5),
                            iaa.ContrastNormalization(0,0.3),
                            iaa.Affine(rotate=(-180,180))
                            )
        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
            img = io.imread(os.path.join(self.data_dir,ID) + '.jpeg')
            X[i,] = crop_and_resize(img,self.dim)
            X[i,] = X[i,].astype(np.float16)
            #X[i,] = transformation(X[i,],self.brightness_range,self.contrast_range)
            # Store class
            y[i] = self.labels[ID]

        if not self.test:
            X = seq.augment_images(X)
        #X = (X-np.mean(X,axis=0))/(np.std(X,axis=0)+1e-7)
        

        return X, y